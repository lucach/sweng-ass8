<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<body>
	<h1>Results of your search</h1>
		
	<ul>
    <c:forEach var="user" items="${users}">
       <li>Username: <c:out value="${user.username}"/>, Name: <c:out value="${user.name}"/> </li>
   </c:forEach>
   </ul>
   
</body>
</html>
