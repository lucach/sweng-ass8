<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<body>
	<h1> Registration </h1>
	<form method="post" action="register">
		Username: <input type="text" name="username"></input> <br/>
		Password: <input type="text" name="password"></input> <br/>
		Name:     <input type="text" name="name"></input> <br/>
		Street:   <input type="text" name="street"></input> <br/>
		City: 	  <input type="text" name="city"></input> <br/>
		Username of best friend, if you want: <input type="text" name="best_friend"></input> <br/>
		<input type="submit"></input>
	</form>
</body>
</html>
