<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<body>
	<h1>Welcome to members area</h1>
	
	<h2>List of users</h2>
	
	<ul>
    <c:forEach var="user" items="${users}">
       <li>Username: <c:out value="${user.username}"/>, Name: <c:out value="${user.name}"/> </li>
   </c:forEach>
   </ul>
   
   <h2>Search</h2>
   <form method="post" action="./search">
   		<input type="hidden" name="search_type" value="name"></input>
   		By Name: <input type="text" name="name"></input>
   		<input type="submit"></input>
   </form>
   <form method="post" action="./search">
   		<input type="hidden" name="search_type" value="bestfriend"></input>
   		By best friend name: <input type="text" name="name"></input>
   		<input type="submit"></input>
   </form>
   <form method="post" action="./search">
   		<input type="hidden" name="search_type" value="address"></input>
   		By address:
   			Street: <input type="text" name="street"></input> 
   			City: <input type="text" name="city"></input>
   		<input type="submit"></input>
   </form>
   
   <h2>Delete</h2>
   <form method="post" action="./delete">
   		By username: <input type="text" name="username"></input>
   		<input type="submit"></input>
   </form>
   
   <h2>Update</h2>
   <form method="post" action="./update">
   		By username: <input type="text" name="username"></input> <br>
		Specify new name: <input type="text" name="new_name"></input>
   		<input type="submit"></input>
   </form>
   
   
</body>
</html>
