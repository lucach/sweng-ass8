package org.chiodini.se.ass8.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/AuthFilter")
public class AuthFilter implements Filter {
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest  req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		HttpSession session = req.getSession();
		boolean authenticated = session.getAttribute("username") != null;
		
		boolean requires_auth = false;
		String path = req.getServletPath();
		if (path != null && path.startsWith("/members_area"))
			requires_auth = true;
		
		if (requires_auth && !authenticated) {
			// Not good, redirect to index
			res.sendRedirect(req.getContextPath() + "/");
		} else {
			// Good, proceed
			chain.doFilter(request, response);
		}
	}



}
