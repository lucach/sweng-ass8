package org.chiodini.se.ass8.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.chiodini.se.ass8.dao.AddressDao;
import org.chiodini.se.ass8.dao.UserDao;
import org.chiodini.se.ass8.model.Address;
import org.chiodini.se.ass8.model.User;


@WebServlet("/members_area/delete")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDao userDao;
	
	public void init() {
		userDao = new UserDao();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter("username");
		userDao.deleteUser(username);
		// go back to list
		response.sendRedirect(".");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}
