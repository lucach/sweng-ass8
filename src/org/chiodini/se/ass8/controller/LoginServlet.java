package org.chiodini.se.ass8.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.chiodini.se.ass8.dao.AddressDao;
import org.chiodini.se.ass8.dao.UserDao;
import org.chiodini.se.ass8.model.Address;
import org.chiodini.se.ass8.model.User;


@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDao userDao;
	
	public void init() {
		userDao = new UserDao();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		User user = userDao.getUser(username);
		if (user != null && user.getPassword().equals(password)) {
			// Login successful
	        HttpSession session = request.getSession();
	        session.setAttribute("username", username);
			response.sendRedirect(request.getContextPath() + "/members_area/");
		} else {
			// Unsuccessful, send back to index
			response.sendRedirect(request.getContextPath() + "/");
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Show login form
		RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
		dispatcher.forward(request, response);
	}
}
