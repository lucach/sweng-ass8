package org.chiodini.se.ass8.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.chiodini.se.ass8.dao.AddressDao;
import org.chiodini.se.ass8.dao.UserDao;
import org.chiodini.se.ass8.model.Address;
import org.chiodini.se.ass8.model.User;


@WebServlet("/register")
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDao userDao;
	private AddressDao addressDao;
	
	public void init() {
		userDao = new UserDao();
		addressDao = new AddressDao();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String street = request.getParameter("street");
		String city = request.getParameter("city");
		Address address = addressDao.getAddressByStreetCity(street, city);
		if (address == null) {
			// New address, if it doesn't exist yet
			address = new Address(request.getParameter("street"), request.getParameter("city"));
			addressDao.saveAddress(address);
		}
		
		// New user

		String name = request.getParameter("name");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		User user = new User();
		user.setName(name);
		user.setUsername(username);
		user.setPassword(password);
		user.setAddress(address);
		
		// Find best friend possibly
		String s_bestFriend = request.getParameter("best_friend");
		User user_bestFriend = userDao.getUser(s_bestFriend);
		if (user_bestFriend != null)
			user.setBestFriend(user_bestFriend);
		
		userDao.saveUser(user);
		response.sendRedirect(request.getContextPath() + "/");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Show registration form
		RequestDispatcher dispatcher = request.getRequestDispatcher("registration.jsp");
		dispatcher.forward(request, response);
	}
}
