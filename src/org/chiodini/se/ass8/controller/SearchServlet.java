package org.chiodini.se.ass8.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.chiodini.se.ass8.dao.AddressDao;
import org.chiodini.se.ass8.dao.UserDao;
import org.chiodini.se.ass8.model.Address;
import org.chiodini.se.ass8.model.User;


@WebServlet("/members_area/search")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDao userDao;
	private AddressDao addressDao;
	
	public void init() {
		userDao = new UserDao();
		addressDao = new AddressDao();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String search_type = request.getParameter("search_type");
		List<User> users = null;
		if (search_type.equals("name")) {
			String name = request.getParameter("name");
			users = userDao.getUsersByName(name);
		} else if (search_type.equals("address")) {
			String street = request.getParameter("street");
			String city = request.getParameter("city");
			Address address = addressDao.getAddressByStreetCity(street, city);
			if (address != null)
				users = userDao.getUsersByAddress(address);
		} else if (search_type.equals("bestfriend")) {
			String name = request.getParameter("name");
			try {
				User bestfriend = userDao.getUsersByName(name).get(0);
				users = userDao.getUsersByBestFriend(bestfriend);
			} catch (Exception e) {};
		}
		request.setAttribute("users", users);
		RequestDispatcher dispatcher = request.getRequestDispatcher("../searchresults.jsp");
		dispatcher.forward(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}
