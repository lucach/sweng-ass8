package org.chiodini.se.ass8.dao;

import java.util.List;

import org.chiodini.se.ass8.model.Address;
import org.chiodini.se.ass8.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class AddressDao {
	
	public void saveAddress(Address address) {
		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			transaction = session.beginTransaction();
			session.save(address);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	public void updateAddress(Address address) {
		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			transaction = session.beginTransaction();
			session.update(address);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	public void deleteAddress(Integer id) {

		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			transaction = session.beginTransaction();
			Address address = session.get(Address.class, id);
			if (address != null) {
				session.delete(address);
			}
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	public Address getAddress(Integer id) {

		Transaction transaction = null;
		Address address = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			transaction = session.beginTransaction();
			address = session.get(Address.class, id);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return address;
	}
		

	@SuppressWarnings("unchecked")
	public Address getAddressByStreetCity(String street, String city) {
		Transaction transaction = null;
		List<Address> addresses = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			transaction = session.beginTransaction();
			addresses = session.createQuery("FROM Address a WHERE a.street = :street AND a.city = :city").setParameter("street", street).setParameter("city", city).getResultList();
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		if (addresses != null && addresses.size() > 0)
			return addresses.get(0);
		else
			return null;
	}
	
	
}
